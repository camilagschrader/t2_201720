package test.model.data_structures;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class RingListTest<T> extends TestCase
{
	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	/**
	 * Lsta desde donde se har�n las pruebas.
	 */
	private RingList<String> ringList;

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------
	/**
	 * Escenario 1: Construye una lista sin nodos.
	 */
	public void setupEscenario1()
	{
		ringList = new RingList<String>();
	}
	
	/**
	 * Escenario 2: Construye una lista con nodos de diferentes tipos.
	 */
	public void setupEscenario2()
	{
		setupEscenario1();
		String mensaje="hola";

		ringList.addFirst(mensaje);
	}
	
	/**
	 * Prueba 1: Prueba el m�todo isEmpty() de la clase RingList.
	 */
	public void testIsEmpty()
	{
		//Caso de prueba 1
		setupEscenario1();
		assertTrue("La lista deber�a estar vacia.", ringList.isEmpty());

		//Caso de prueba 2
		setupEscenario2();
		assertFalse("La lista no deber�a estar vacia.", ringList.isEmpty());
	}
	
	/**
	 * Prueba 2: Prueba el m�todo addFirst() de la clase RingList.
	 */
	public void testAddFirst()
	{
		setupEscenario2();

		String mensaje="c�mo";

		ringList.addFirst(mensaje);
		assertEquals("El primer elemento deber�a corresponder a c�mo", "c�mo", ringList.getElement(0));
	}
	
	/**
	 * Prueba 3: Prueba el m�todo getNodeByPosition() de la clase RingList.
	 */
	public void testGetNodeByPosition()
	{
		//Caso de Prueba 1
		setupEscenario1();

		try
		{
			ringList.getNodeByPosition(6);
			fail( "Deber�a lanzar excepci�n." );
		}
		catch(Exception e)
		{
			//Deber�a pasar por ac�
		}

		//Caso de Prueba 2
		setupEscenario2();

		try
		{
			assertEquals("Deber�a retornar el elemento hola", "hola", ringList.getNodeByPosition(0).darElemento());
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}
		
		//Caso de Prueba 3
		try
		{
			setupEscenario2();
			String mensaje="c�mo";
			ringList.addAtEnd(mensaje);
			assertEquals("Deber�a retornar el elemento hola", "hola", ringList.getNodeByPosition(2).darElemento());
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}

	}
	
	/**
	 * Prueba 4: Prueba el m�todo getNodeByElement() de la clase RingList.
	 */
	public void testGetNodeByElement()
	{
		//Caso de Prueba 1
		setupEscenario1();

		try
		{
			ringList.getNodeByElement("estas");
			fail( "Deber�a lanzar excepci�n." );
		}
		catch(Exception e)
		{
			//Deber�a pasar por ac�
		}

		//Caso de Prueba 2
		setupEscenario2();

		try
		{
			assertEquals("Deber�a retornar el elemento hola", "hola", ringList.getNodeByElement("hola").darElemento());
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}
	}
	
	/**
	 * Prueba 5: Prueba el m�todo addAtEnd() de la clase RingList.
	 */

	public void testAddAtEnd()
	{
		String mensaje="c�mo";

		//Caso de prueba 1.
		setupEscenario1();
		ringList.addAtEnd(mensaje);
		assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getFirst().darElemento());
		assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getFirst().darAnterior().darElemento());

		//Caso de prueba 2.
		setupEscenario2();
		ringList.addAtEnd(mensaje);
		assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(1));
		assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getFirst().darAnterior().darElemento());

	}
	
	/**
	 * Prueba 6: Prueba el m�todo add() de la clase RingList.
	 */

	public void testAdd()
	{
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		ringList.addAtEnd(mensaje);

		try 
		{
			Node<String> help=ringList.getNodeByPosition(0);
			ringList.updateActual(help);
			ringList.add(mensaje2);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(1));

		} 
		catch (Exception e)
		{
			fail("No deber�a lanzar exception");

		}

	}
	
	/**
	 * Prueba 7: Prueba el m�todo addAtK() de la clase DoubleLinkedList.
	 */
	public void testAddAtK()
	{
		setupEscenario1();
		String mensaje3="estas";

		//Caso 1
		try
		{
			ringList.addAtk(mensaje3, 6);
			fail("Deber�a lanzar exception");
		}
		catch(Exception e)
		{
			//Deber�a pasar por ac�.
		}

		//Caso 2
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		ringList.addAtEnd(mensaje);
		
		try
		{
			ringList.addAtk(mensaje2, 1);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(1));
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}
		
		//Caso 3
		setupEscenario2();
		ringList.addAtEnd(mensaje);
		
		try
		{
			ringList.addAtk(mensaje2, 3);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(1));
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}
	}
	
	/**
	 * Prueba 8: Prueba el m�todo getCurrentElement() de la clase RingList.
	 */
	public void testGetCurrentElement()
	{
		//Caso 2
		setupEscenario1();
		try 
		{
			ringList.getCurrentElement();
			fail("Deber�a lanzar excepci�n");
		} 
		catch (Exception e)
		{
			//Deber�a pasar por ac�.
		}

		//Caso 2
		setupEscenario2();
		try 
		{
			Node<String> help=ringList.getNodeByPosition(0);
			ringList.updateActual(help);
			assertEquals("El elemento deber�a ser hola", "hola", ringList.getCurrentElement());
		} 
		catch (Exception e)
		{
			fail("No deber�a lanzar exception");
		}
	}
	
	/**
	 * Prueba 9: Prueba el m�todo getSize() de la clase ringList.
	 */
	public void testGetSize()
	{
		setupEscenario1();
		assertEquals("El tama�o deber�a ser 0", 0, ringList.getSize());
	}
	
	/**
	 * Prueba 10: Prueba el m�todo delete() de la clase ringList.
	 */
	public void testDelete()
	{
		//Caso 1
		setupEscenario1();
		
		try
		{
			ringList.delete();
			fail("Deber�a lanzar exception");
		}
		catch(Exception e)
		{
			//Deber�a entrar ac�.
		}
		
		//Caso 2
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		ringList.addAtEnd(mensaje2);
		ringList.addAtEnd(mensaje);
		
		try 
		{
			Node<String> help = ringList.getNodeByPosition(0);
			ringList.updateActual(help);
			ringList.delete();
			assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(0));
		} 
		catch (Exception e) 
		{
			fail("No deber�a lanzar exception");
		}
	}
	
	/**
	 * Prueba 11: Prueba el m�todo deleteAtk() de la clase ringList.
	 */
	
	public void testDeleteAtk()
	{
		//Caso 1
		setupEscenario1();
		
		try
		{
			ringList.deleteAtk(0);
			fail("Deber�a lanzar exception");
		}
		catch(Exception e)
		{
			//Deber�a entrar ac�.
		}
		
		//Caso 2
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		ringList.addAtEnd(mensaje2);
		ringList.addAtEnd(mensaje);
		

		try 
		{
			Node<String> help = ringList.getNodeByPosition(0);
			ringList.updateActual(help);
			ringList.deleteAtk(0);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(0));
		} 
		catch (Exception e) 
		{
			fail("No deber�a lanzar exception");
		}
		
		//Caso 3
		setupEscenario2();
		ringList.addAtEnd(mensaje2);
		ringList.addAtEnd(mensaje);
		
		try 
		{
			Node<String> help = ringList.getNodeByPosition(0);
			ringList.updateActual(help);
			ringList.deleteAtk(3);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", ringList.getElement(0));
		} 
		catch (Exception e) 
		{
			fail("No deber�a lanzar exception");
		}
	}


}
