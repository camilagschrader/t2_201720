package model.vo;

import model.data_structures.DoubleLinkedList;

public class VOZone {
	//ATRIBUTOS
	private String id;

	private DoubleLinkedList<VOStop> paradas;

	//CONSTRUCTOR
	public VOZone(String pID, DoubleLinkedList<VOStop> pParadas)
	{
		id = pID;
		paradas = pParadas;
	}

	//METODOS
	/**
	 * 
	 * @return id - zone id
	 */
	public String getID()
	{
		return id;
	}

	/**
	 * 
	 * @return paradas - las paradas de la zona.
	 */
	public DoubleLinkedList<VOStop> getParadas()
	{
		return paradas;
	}
}


