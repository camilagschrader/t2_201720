package model.vo;

import model.data_structures.DoubleLinkedList;

/**
 * Clase para modelar una ruta que guarda sus viajes en una lista.
 *
 */
public class VORouteWTrips {

	//ATRIBUTOS
	private int id;

	private DoubleLinkedList<VOTripInRoute> viajes;

	//CONSTRUCTOR
	public VORouteWTrips(int pID, DoubleLinkedList<VOTripInRoute> pViajes)
	{
		id = pID;
		viajes = pViajes;
	}

	//METODOS
	/**
	 * 
	 * @return id - route id
	 */
	public int getID()
	{
		return id;
	}

	/**
	 * 
	 * @return viajes - los viajes de la ruta
	 */
	public DoubleLinkedList<VOTripInRoute> getViajes()
	{
		return viajes;
	}
}
