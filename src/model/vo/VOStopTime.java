package model.vo;

/**
 * Representation of a stop time object
 */
public class VOStopTime 
{
	//ATRIBUTOS
	private int tripID;

	private String arrivalTime;

	private String departureTime;

	private int stopID;

	private int stopSequence;

	private String stopHeadsign;

	private int pickUpType;

	private int dropOffType;

	private String distTraveled;

	//CONSTRUCTOR
	public VOStopTime(int pTripID, String pArrivalTime, String pDepartureTime, int pStopID, int pStopSequence, String pStopHeadsign, int pPickupType, int pDropOffType, String pDistTraveled)
	{
		tripID = pTripID;
		arrivalTime = pArrivalTime;
		departureTime = pDepartureTime;
		stopID = pStopID;
		stopSequence = pStopSequence;
		stopHeadsign = pStopHeadsign;
		pickUpType = pPickupType;
		dropOffType = pDropOffType;
		distTraveled = pDistTraveled;
	}

	//METODOS
	/**
	 * @return TripID - stop's trip id
	 */
	public int getTripID() {

		return tripID;
	}

	/**
	 * @return arrivalTime - stop's arrival time
	 */
	public String getArrivalTime() {

		return arrivalTime;
	}

	/**
	 * @return departureTime - stop's departure time
	 */
	public String getDepartureTime() {

		return departureTime;
	}

	/**
	 * @return stopID - stop id
	 */
	public int getStopID() {

		return stopID;
	}

	/**
	 * @return stopSequence - stop sequence
	 */
	public int getStopSequence() {

		return stopSequence;
	}

	/**
	 * @return stopHeadsign - stop's headsign
	 */
	public String getStopHeadsign() {

		return stopHeadsign;
	}

	/**
	 * @return pickUpType - stop's pickup type
	 */
	public int getPickUpType() {

		return pickUpType;
	}

	/**
	 * @return dropOffType - stop's dropOff type
	 */
	public int getDropOffType() {

		return dropOffType;
	}

	/**
	 * @return distTraveled - stop's distance traveled
	 */
	public String getDistTraveled() {

		return distTraveled;
	}
}
