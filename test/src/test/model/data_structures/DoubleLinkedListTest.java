package test.model.data_structures;

import junit.framework.TestCase;
import model.data_structures.DoubleLinkedList;
import model.data_structures.Node;

/**
 * Clase usada para verificar que los m�todos de la clase DoubleLinkedList est�n correctamente implementados.
 */
public class DoubleLinkedListTest<T> extends TestCase{

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	/**
	 * Lsta desde donde se har�n las pruebas.
	 */
	private DoubleLinkedList<String> doubleLinkedList;

	// -------------------------------------------------------------
	// M�todos
	// -------------------------------------------------------------

	/**
	 * Escenario 1: Construye una lista sin nodos.
	 */
	public void setupEscenario1()
	{
		doubleLinkedList = new DoubleLinkedList<String>();
	}

	/**
	 * Escenario 2: Construye una lista con nodos de diferentes tipos.
	 */
	public void setupEscenario2()
	{
		setupEscenario1();
		String mensaje="hola";

		doubleLinkedList.addFirst(mensaje);
	}

	/**
	 * Prueba 1: Prueba el m�todo isEmpty() de la clase DoubleLinkedList.
	 */
	public void testIsEmpty()
	{
		//Caso de prueba 1
		setupEscenario1();
		assertTrue("La lista deber�a estar vacia.", doubleLinkedList.isEmpty());

		//Caso de prueba 2
		setupEscenario2();
		assertFalse("La lista no deber�a estar vacia.", doubleLinkedList.isEmpty());
	}

	/**
	 * Prueba 2: Prueba el m�todo addFirst() de la clase DoubleLinkedList.
	 */
	public void testAddFirst()
	{
		setupEscenario2();

		String mensaje="c�mo";

		doubleLinkedList.addFirst(mensaje);
		assertEquals("El primer elemento deber�a corresponder a c�mo", "c�mo", doubleLinkedList.getElement(0));
	}

	/**
	 * Prueba 3: Prueba el m�todo getNodeByPosition() de la clase DoubleLinkedList.
	 */
	public void testGetNodeByPosition()
	{
		//Caso de Prueba 1
		setupEscenario1();

		try
		{
			doubleLinkedList.getNodeByPosition(6);
			fail( "Deber�a lanzar excepci�n." );
		}
		catch(Exception e)
		{
			//Deber�a pasar por ac�
		}

		//Caso de Prueba 2
		setupEscenario2();

		try
		{
			assertEquals("Deber�a retornar el elemento hola", "hola", doubleLinkedList.getNodeByPosition(0).darElemento());
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}

	}

	/**
	 * Prueba 4: Prueba el m�todo getNodeByElement() de la clase DoubleLinkedList.
	 */
	public void testGetNodeByElement()
	{
		//Caso de Prueba 1
		setupEscenario1();

		try
		{
			doubleLinkedList.getNodeByElement("estas");
			fail( "Deber�a lanzar excepci�n." );
		}
		catch(Exception e)
		{
			//Deber�a pasar por ac�
		}

		//Caso de Prueba 2
		setupEscenario2();

		try
		{
			assertEquals("Deber�a retornar el elemento hola", "hola", doubleLinkedList.getNodeByElement("hola").darElemento());
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}
	}

	/**
	 * Prueba 5: Prueba el m�todo addAtEnd() de la clase DoubleLinkedList.
	 */

	public void testAddAtEnd()
	{
		String mensaje="c�mo";

		//Caso de prueba 1.
		setupEscenario1();
		doubleLinkedList.addAtEnd(mensaje);
		assertEquals("El elemento deber�a ser c�mo", "c�mo", doubleLinkedList.getElement(0));

		//Caso de prueba 2.
		setupEscenario2();
		doubleLinkedList.addAtEnd(mensaje);
		assertEquals("El elemento deber�a ser c�mo", "c�mo", doubleLinkedList.getElement(1));
	}

	/**
	 * Prueba 6: Prueba el m�todo add() de la clase DoubleLinkedList.
	 */

	public void testAdd()
	{
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		doubleLinkedList.addAtEnd(mensaje);

		try 
		{
			Node<String> help=doubleLinkedList.getNodeByPosition(0);
			doubleLinkedList.updateActual(help);
			doubleLinkedList.add(mensaje2);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", doubleLinkedList.getElement(1));

		} 
		catch (Exception e)
		{
			fail("No deber�a lanzar exception");

		}

	}


	/**
	 * Prueba 7: Prueba el m�todo addAtK() de la clase DoubleLinkedList.
	 */
	public void testAddAtK()
	{
		setupEscenario2();

		String mensaje="estas";
		String mensaje2="c�mo";
		doubleLinkedList.addAtEnd(mensaje);

		//Caso 1
		try
		{
			doubleLinkedList.addAtk(mensaje2, 6);
			fail("Deber�a lanzar exception");
		}
		catch(Exception e)
		{
			//Deber�a pasar por ac�.
		}

		//Caso 2
		try
		{
			doubleLinkedList.addAtk(mensaje2, 1);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", doubleLinkedList.getElement(1));
		}
		catch(Exception e)
		{
			fail("No deber�a lanzar exception");
		}
	}

	/**
	 * Prueba 8: Prueba el m�todo getCurrentElement() de la clase DoubleLinkedList.
	 */
	public void testGetCurrentElement()
	{
		//Caso 2
		setupEscenario1();
		try 
		{
			doubleLinkedList.getCurrentElement();
			fail("Deber�a lanzar excepci�n");
		} 
		catch (Exception e)
		{
			//Deber�a pasar por ac�.
		}

		//Caso 2
		setupEscenario2();
		try 
		{
			Node<String> help=doubleLinkedList.getNodeByPosition(0);
			doubleLinkedList.updateActual(help);
			assertEquals("El elemento deber�a ser hola", "hola", doubleLinkedList.getCurrentElement());
		} 
		catch (Exception e)
		{
			fail("No deber�a lanzar exception");
		}
	}

	/**
	 * Prueba 9: Prueba el m�todo getSize() de la clase DoubleLinkedList.
	 */
	public void testGetSize()
	{
		setupEscenario1();
		assertEquals("El tama�o deber�a ser 0", 0, doubleLinkedList.getSize());
	}

	/**
	 * Prueba 10: Prueba el m�todo delete() de la clase DoubleLinkedList.
	 */
	public void testDelete()
	{
		//Caso 1
		setupEscenario1();
		
		try
		{
			doubleLinkedList.delete();
			fail("Deber�a lanzar exception");
		}
		catch(Exception e)
		{
			//Deber�a entrar ac�.
		}
		
		//Caso 2
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		doubleLinkedList.addAtEnd(mensaje2);
		doubleLinkedList.addAtEnd(mensaje);
		
		try 
		{
			Node<String> help = doubleLinkedList.getNodeByPosition(0);
			doubleLinkedList.updateActual(help);
			doubleLinkedList.delete();
			assertEquals("El elemento deber�a ser c�mo", "c�mo", doubleLinkedList.getElement(0));
		} 
		catch (Exception e) 
		{
			fail("No deber�a lanzar exception");
		}
	}

	/**
	 * Prueba 11: Prueba el m�todo deleteAtk() de la clase DoubleLinkedList.
	 */
	
	public void testDeleteAtk()
	{
		//Caso 1
		setupEscenario1();
		
		try
		{
			doubleLinkedList.deleteAtk(0);
			fail("Deber�a lanzar exception");
		}
		catch(Exception e)
		{
			//Deber�a entrar ac�.
		}
		
		//Caso 2
		setupEscenario2();
		String mensaje="estas";
		String mensaje2="c�mo";
		doubleLinkedList.addAtEnd(mensaje2);
		doubleLinkedList.addAtEnd(mensaje);
		

		try 
		{
			Node<String> help = doubleLinkedList.getNodeByPosition(0);
			doubleLinkedList.updateActual(help);
			doubleLinkedList.deleteAtk(0);
			assertEquals("El elemento deber�a ser c�mo", "c�mo", doubleLinkedList.getElement(0));
		} 
		catch (Exception e) 
		{
			fail("No deber�a lanzar exception");
		}
	}
}
