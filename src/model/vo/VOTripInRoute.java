package model.vo;

public class VOTripInRoute {

	//ATRIBUTOS
	private int serviceID;

	private int tripID;

	private String tripHeadsign;

	private String tripShortName;

	private int directionID;

	private int blockID;

	private int shapeID;

	private int wheelchairAccessible;

	private int bikesAllowed;

	//CONSTRUCTOR
	public VOTripInRoute( int pServiceID, int pTripID, String pTripHeadsign, String pTripShortName, int pDirectionID, int pBlockID, int pShapeID, int pWheelchairAccessible, int pBikesAllowed)
	{
		serviceID = pServiceID;
		tripID = pTripID;
		tripHeadsign = pTripHeadsign;
		tripShortName = pTripShortName;
		directionID = pDirectionID;
		blockID = pBlockID;
		shapeID = pShapeID;
		wheelchairAccessible = pWheelchairAccessible;
		bikesAllowed = pBikesAllowed;
	}

	//METODOS

	/**
	 * @return serviceID - trip's service id
	 */
	public int getServiceID() {

		return serviceID;
	}

	/**
	 * @return tripID - trip's id
	 */
	public int getTripID() {

		return tripID;
	}

	/**
	 * @return tripHeadsign - trip's headsign
	 */
	public String getTripHeadsign() {

		return tripHeadsign;
	}

	/**
	 * @return tripShortName - trip's short name
	 */
	public String getTripShortName() {

		return tripShortName;
	}

	/**
	 * @return directionID - trip's direction id
	 */
	public int getTripDirectionID() {

		return directionID;
	}

	/**
	 * @return blockID - trip's block id
	 */
	public int getBlockID() {

		return blockID;
	}

	/**
	 * @return shapeID - trip's shape id
	 */
	public int getShapeID() {

		return shapeID;
	}

	/**
	 * @return wheelchairAccessible - trip's wheelchair accessible condition
	 */
	public int getWheelchairAccessible() {

		return wheelchairAccessible;
	}

	/**
	 * @return bikesAllowed - trip's bikes allowed condition
	 */
	public int getBikesAllowed() {

		return bikesAllowed;
	}

}
