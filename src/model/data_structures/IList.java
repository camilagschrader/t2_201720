package model.data_structures;

/**
 * Abstract Data Type for a list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IList<T> extends Iterable<T> {

	
	public void add(T element)throws Exception;
	
	public void addAtEnd(T element);
	
	public T getElement(int position);
	
	public T getCurrentElement();
	
	public int getSize();
	
	public void delete() throws Exception;
	
	public Node<T> next();
	
	public Node<T> previous();

	void addAtk(T element, int position) throws Exception;
	
	void deleteAtk(int position)throws Exception;
	
	
}
