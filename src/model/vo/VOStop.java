package model.vo;

import java.sql.Time;

/**
 * Representation of a Stop object
 */
public class VOStop {

	//ATRIBUTOS
	private int id;

	private String code;

	private String name;

	private String desc;

	private double latitude;

	private double longitude;

	private String zone;

	private String url;

	private String locationType;

	private String parentStation;

	//CONSTRUCTOR
	public VOStop(int pID, String pCode, String pName, String pDesc, double pLatitude, double pLongitude, String pZone, String pUrl, String pLocationType, String pParentStation)
	{
		id = pID;
		code = pCode;
		name = pName;
		desc = pDesc;
		latitude = pLatitude;
		longitude = pLongitude;
		zone = pZone;
		url = pUrl;
		locationType = pLocationType;
		parentStation = pParentStation;
	}

	//METODOS
	/**
	 * @return id - stop's id
	 */
	public int getId() {

		return id;
	}

	/**
	 * @return code - stop's code
	 */
	public String getCode() {

		return code;
	}


	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	/**
	 * @return desc - stop's desc
	 */
	public String getDesc() {

		return desc;
	}

	/**
	 * @return latitude - stop's latitude
	 */
	public double getLatitude() {

		return latitude;
	}

	/**
	 * @return longitude - stop's longitude
	 */
	public double getLongitude() {

		return longitude;
	}

	/**
	 * @return url - stop's url
	 */
	public String getUrl() {

		return url;
	}

	/**
	 * @return locationType - stop's location type
	 */
	public String getLocationType() {

		return locationType;
	}

	/**
	 * @return parentStation - stop's parent station
	 */
	public String getParentStation() {

		return parentStation;
	}

}
