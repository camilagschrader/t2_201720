package model.data_structures;

import java.util.Iterator;


public class DoubleLinkedList<T> implements IList<T>
{
	//ATRIBUTOS

	private Node<T> first;

	private Node<T> last;

	private Node<T> actual;

	private int size;


	public DoubleLinkedList()
	{
		first = null;
		last = null;
		actual=null;
		size = 0;
	}

	public Node<T> getFirst()
	{
		return first;
	}

	public Node<T> getLast()
	{
		return last;
	}
	
	public Node<T> getActual()
	{
		return actual;
	}

	public void updateActual(Node<T> pActual)
	{
		actual=pActual;
	}

	public boolean isEmpty()
	{
		return size == 0;
	}

	public void addFirst(T element)
	{
		Node<T> current = new Node<T>(element, null, first);
		if(first != null)
			first.cambiarAnterior(current);
		first = current;
		if(last == null )
			last = current;
		size++;
	}

	public Node<T> getNodeByPosition(int position) throws Exception
	{
		if(isEmpty()|| position>size-1)
		{
			throw new Exception("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");
		}

		else if (position==0)
		{
			return first;
		}

		else if(position==size-1)
		{
			return last;
		}
		else
		{
			int counter=1;
			actual=first.darSiguiente();
			while(counter<=position && actual!=null)
			{
				if(counter==position)
				{
					return actual;
				}

				counter++;
				actual=actual.darSiguiente();
			}
		}

		return null;
	}

	public Node<T> getNodeByElement(T element) throws Exception
	{

		if(isEmpty())
		{
			throw new Exception("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");
		}

		else if (first.darElemento()==element)
		{
			return first;
		}

		else if(last.darElemento()==element)
		{
			return last;
		}
		else
		{
			int counter=1;
			actual=first.darSiguiente();
			while(actual!=null&& counter<size)
			{
				if(actual.darElemento()==element)
				{
					return actual;
				}
				actual=actual.darSiguiente();
			}
			counter++;
		}

		return null;
	}


	public void addAtEnd(T element) {
		Node<T> current = new Node<T>(element, last, null);
		if(last != null)
			last.cambiarSiguiente(current);
		last = current;
		if(first == null)
			first = current;
		size++;

	}

	public Iterator iterator() {
		return new Iterator<T>() {

			public boolean hasNext() {
				if(actual.darSiguiente()!=null)
				{
					return true;
				}
				return false;
			}

			public T next() {
				return actual.darSiguiente().darElemento();
			}
		};
	}

	public void add(T element) throws Exception {
		if(actual!=null)
		{
			Node<T> current=new Node<T>(element, actual, actual.darSiguiente());
			if(isEmpty())
			{
				first=current;
				last=current;
			}
			else if(actual==first)
			{
				if(first.darSiguiente()!=null)
				{
					first.darSiguiente().cambiarAnterior(current);
				}
				else
				{
					last=current;
				}
				
			}
			else if(actual==last)
			{
				last=current;
				
			}
			else
			{
				if(actual.darSiguiente()!=null)
					actual.darSiguiente().cambiarAnterior(current);
				
			}
			actual.cambiarSiguiente(current);
			size++;
		}
		else
		{
			throw new Exception("No se puede agregar desde este elemento, pues es nulo");
		}

	}

	public void addAtk(	T element, int position) throws Exception {

		if(isEmpty() && position==0)
		{
			Node<T> current=new Node<T>(element, null, null);
			first=current;
			last=current;
			size++;
		}
		else if(position>size)
		{
			throw new  Exception("Se quiere agregar un Nodo en una posici�n mayor al tama�o actual de la lista. No se agrego ningun Nodo");

		}
		else if(position==0)
		{
			addFirst(element);
			
		}

		else if(position==size)
		{
			addAtEnd(element);
			
		}
		else
		{
			try
			{
				Node<T> previous=getNodeByPosition(position-1);
				Node<T> next=getNodeByPosition(position);

				Node<T> current=new Node<T>(element, previous, next);

				previous.cambiarSiguiente(current);
				next.cambiarAnterior(current);
				size++;
			}
			catch(Exception e)
			{
				System.out.println("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");
				return;
			}
		}

	}

	public T getElement(int position) {
		try
		{
			T elementT=getNodeByPosition(position).darElemento();
			return elementT;
		}
		catch(Exception e)
		{
			//System.out.println("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");

		}
		return null;
	}

	public T getCurrentElement() {
		return actual.darElemento();
	}

	public int getSize() {
		return size;
	}

	public void delete() throws Exception {

		if(isEmpty())
		{
			throw new Exception("No hay ning�n elemento para eliminar.");
		}
		else
		{
			if(actual==first)
			{
				first=actual.darSiguiente();
				first.cambiarAnterior(null);
			}
			else if(actual==last)
			{
				last=actual.darAnterior();
				last.cambiarSiguiente(null);
			}
			else
			{
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				actual.darSiguiente().cambiarAnterior(actual.darAnterior());
			}
			size--;
		}
	}
	public Node<T> next() {
		return actual.darSiguiente();
	}

	public Node<T> previous() {
		return actual.darAnterior();
	}

	public void deleteAtk(int position) throws Exception {
		if(isEmpty())
		{
			throw new Exception("No hay ning�n elemento para eliminar.");
		}
		else
		{
			try
			{
				actual=getNodeByPosition(position);

				if(actual==first)
				{
					Node<T> help=first.darSiguiente();
					first.darSiguiente().cambiarAnterior(null);
					first.cambiarSiguiente(null);
					first=help;
				}

				if(actual==last)
				{
					Node<T> help=last.darAnterior();
					last.darAnterior().cambiarSiguiente(null);
					last.cambiarAnterior(null);
					last=help;
				}
				else
				{
					actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
					actual.darSiguiente().cambiarAnterior(actual.darAnterior());
					actual.cambiarAnterior(null);
					actual.cambiarSiguiente(null);
				}
				size--;
			}
			catch(Exception e)
			{
				System.out.println("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");
				return;
			}
		}
	}

}
