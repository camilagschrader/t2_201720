package model.data_structures;

import java.util.Iterator;

public class RingList<T> implements IList<T>{

	//ATRIBUTOS

	private Node<T> first;

	private Node<T> actual;

	private int size;


	public RingList()
	{
		first = null;
		size = 0;

	}
	
	public Node<T> getFirst()
	{
		return first;
	}
	
	public Node<T> getActual()
	{
		return actual;
	}
	
	public void updateActual(Node<T> pActual)
	{
		actual=pActual;
	}

	public boolean isEmpty()
	{
		return size == 0;
	}

	public void addFirst(T element)
	{
		if(first != null)
		{
			Node<T> current = new Node<T>(element, first.darAnterior(), first);
			first.darAnterior().cambiarSiguiente(current);
			first.cambiarAnterior(current);
			first=current;
		}
		else
		{
			Node<T> current = new Node<T>(element, first, first);
			first = current;
			current.cambiarSiguiente(first);
			current.cambiarAnterior(first);
		}
		size++;
	}

	public Node<T> getNodeByPosition(int position) throws Exception
	{
		int counterP=position;
		
		if(isEmpty()&&position>0)
		{
			throw new Exception("La lista se encuentra vac�a, se esta pidiendo una posici�n vacia");
			
		}
		
		if(position>size-1)
		{
			counterP=position%(size);	
		}

		if (counterP==0)
		{
			return first;
		}

		else if(counterP==size-1)
		{
			return first.darAnterior();
		}
		else
		{
			int counter=1;
			actual=first.darSiguiente();
			while(actual.darSiguiente()!=first)
			{
				if(counter==counterP)
				{
					return actual;
				}

				counter++;
				actual=actual.darSiguiente();
			}
		}

		return null;
	}

	public Node<T> getNodeByElement(T element) throws Exception
	{
		if(isEmpty())
		{
			throw new Exception("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");
		}

		else if (first.darElemento()==element)
		{
			return first;
		}

		else if(first.darAnterior().darElemento()==element)
		{
			return first.darAnterior();
		}

		else
		{
			Node<T> current=first.darSiguiente();
			while(current!=first)
			{
				if(current.darElemento()==element)
				{
					return current;
				}
				current=current.darSiguiente();
			}
		}

		return null;
	}


	public void addAtEnd(T element) {
		if(first != null)
		{
			Node<T> current = new Node<T>(element, first.darAnterior(), null);
			first.darAnterior().cambiarSiguiente(current);
			first.cambiarAnterior(current);
		}
		else if(first == null)
		{
			Node<T> current = new Node<T>(element, null, null);
			first = current;
			current.cambiarAnterior(first);
			current.cambiarSiguiente(first);
		}

		size++;

	}

	public Iterator iterator() {
		return new Iterator<T>() {

			public boolean hasNext() {
				if(isEmpty())
				{
					return false;
				}
				return true;
			}

			public T next() {
				return actual.darSiguiente().darElemento();
			}
		};
	}

	public void add(T element) {
		if(isEmpty())
		{
			Node<T> current=new Node<T>(element, null, null);
			first=current;
			current.cambiarAnterior(first);
			current.cambiarSiguiente(first);
			size ++;

		}
		else
		{
			Node<T> current=new Node<T>(element, actual, actual.darSiguiente());
			if(actual.darSiguiente()!=null)
				actual.darSiguiente().cambiarAnterior(current);
			actual.cambiarSiguiente(current);
			size++;
		}
	}

	public void addAtk(	T element, int position) throws Exception {

		int counter=position;

		if(isEmpty()&&position>0)
		{
			throw new Exception("La lista se encuentra vac�a o se esta pidiendo una posici�n mas grande que el n�mero de elementos.");

		}

		if(position>size-1)
		{
			counter=position%(size);	
		}

		if(counter==0)
		{
			addFirst(element);
			
		}

		else if(counter==size)
		{
			addAtEnd(element);
			
		}
		else
		{
			try 
			{
				Node<T> previous= getNodeByPosition(counter-1);
				Node<T> next=getNodeByPosition(counter);

				Node<T> current=new Node<T>(element, previous, next);

				previous.cambiarSiguiente(current);
				next.cambiarAnterior(current);
				size++;
			} 
			catch (Exception e) 
			{
				System.out.println("La lista se encuentra vac�a, se quiere obtener una posici�n inexistente.");
				return;
			}
		}

	}

	public T getElement(int position) {

		try
		{
			T elementT = getNodeByPosition(position).darElemento();
			return elementT;
		}
		catch (Exception e)
		{
			System.out.println("La lista se encuentra vac�a, se quiere obtener una posici�n inexistente.");
		}
		return null;
	}

	public T getCurrentElement() {
		return actual.darElemento();
	}

	public int getSize() {
		return size;
	}

	public void delete() throws Exception {
		if(isEmpty())
		{
			throw new Exception("No hay ning�n elemento para eliminar.");
		}
		else
		{
			Node<T> help=first.darAnterior();

			if(actual==first)
			{
				if(first.darAnterior()!=first)
				{
					first.darAnterior().cambiarSiguiente(actual.darSiguiente());
					first=actual.darSiguiente();
					first.cambiarAnterior(help);
				}
				else
				{
					first=null;
				}
			}			
			else if(actual==first.darAnterior())
			{
				actual.darAnterior().cambiarSiguiente(first);
				first.cambiarAnterior(actual.darAnterior());

			}
			else
			{
				actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
				actual.darSiguiente().cambiarAnterior(actual.darAnterior());
			}
			size--;
		}
	}
	public Node<T> next() {
		return actual.darSiguiente();
	}

	public Node<T> previous() {
		return actual.darAnterior();
	}

	public void deleteAtk(int position) throws Exception {
		if(isEmpty())
		{
			throw new Exception("No hay ning�n elemento para eliminar.");
		}
		else
		{
			Node<T> current=getNodeByPosition(position);
			Node<T> help=first.darAnterior();

			if(current==first)
			{
				if(first.darAnterior()!=first)
				{
					first.darAnterior().cambiarSiguiente(current.darSiguiente());
					first=current.darSiguiente();
					first.cambiarAnterior(help);
				}
				else
				{
					first=null;
				}
			}			
			else if(current==first.darAnterior())
			{
				current.darAnterior().cambiarSiguiente(first);
				first.cambiarAnterior(current.darAnterior());

			}
			else
			{
				current.darAnterior().cambiarSiguiente(current.darSiguiente());
				current.darSiguiente().cambiarAnterior(current.darAnterior());
			}
			size--;
		}
	}


}
