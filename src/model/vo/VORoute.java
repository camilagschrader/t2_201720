package model.vo;

/**
 * Representation of a route object
 */
public class VORoute 
{
	//ATRIBUTOS

	private int id;

	private String agency;

	private String shortName;

	private String longName;

	private String desc;

	private int type;

	private String url;

	private String color;

	private String textColor;

	//CONSTRUCTOR
	public VORoute(int pId, String pAgency, String pShortName, String pLongName, String pDesc, int pType, String pUrl, String pColor, String pTextColor)
	{
		id = pId;
		agency = pAgency;
		shortName = pShortName;
		longName = pLongName;
		desc = pDesc;
		type = pType;
		url = pUrl;
		color = pColor;
		textColor = pTextColor;
	}

	/**
	 * @return id - Route's id number
	 */
	public int getId() 
	{
		// TODO Auto-generated method stub
		return id;
	}

	//METODOS
	/**
	 * @return shortName - route name
	 */
	public String getName() 
	{
		// TODO Auto-generated method stub
		return shortName;
	}

	/**
	 * @return longName
	 */
	public String getLongName() 
	{
		return longName;
	}

	/**
	 * @return desc
	 */
	public String getDesc() 
	{
		return desc;
	}

	/**
	 * @return type - route type
	 */
	public int getType() 
	{
		return type;
	}

	/**
	 * @return url - route url
	 */
	public String getUrl() 
	{
		return url;
	}

	/**
	 * @return color - route color
	 */
	public String getColor() 
	{
		return color;
	}

	/**
	 * @return textColor - route textColor
	 */
	public String getTextColor() 
	{
		return textColor;
	}

}
