package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import api.ISTSManager;
import model.vo.VORoute;
import model.vo.VORouteWTrips;
import model.vo.VOStop;
import model.vo.VOStopTime;
import model.vo.VOTripInRoute;
import model.vo.VOZone;
import sun.net.NetworkServer;
import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.Node;
import model.data_structures.RingList;

public class STSManager implements ISTSManager {

	public static final String SPLIT_BY = ",";



	@Override
	public void loadRoutes(String routesFile) 
	{
		// TODO Auto-generated method stub
		BufferedReader br;
		String line = "";
		try 
		{
			br = new BufferedReader(new FileReader(routesFile));
			DoubleLinkedList<VORoute> listaRoutes = new DoubleLinkedList<VORoute>();
			line = br.readLine();
			while ((line = br.readLine()) != null) 
			{
				String[] ruta = line.split(SPLIT_BY);

				VORoute newRoute = new VORoute(Integer.parseInt(ruta[0]), ruta[1], ruta[2], ruta[3], ruta[4], Integer.parseInt(ruta[5]), ruta[6], ruta[7], ruta[8]);
				listaRoutes.addAtEnd(newRoute);
			} 
			br.close();

		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	public Node<VORouteWTrips> findId(int pId, RingList<VORouteWTrips> pLista)
	{
		Node<VORouteWTrips> current=pLista.getFirst();
		if(current!=null)
		{
			if(current.darElemento().getID()==pId)
			{
				return current;
			}
			current=current.darSiguiente();


			while(current!=pLista.getFirst() && current!=null)
			{
				if(current.darElemento().getID()==pId)
				{
					return current;
				}
				current=current.darSiguiente();
			}
		}
		else
		{
			return null;	
		}
		return null;
	}

	@Override
	public void loadTrips(String tripsFile)
	{
		FileReader lector;
		RingList<VORouteWTrips> listaRutas = new RingList<VORouteWTrips>();
		try 
		{
			lector = new FileReader( tripsFile );
			BufferedReader leclinea= new BufferedReader( lector );
			String linea=leclinea.readLine( );

			linea=leclinea.readLine( );

			while(linea!=null)
			{
				String[] rutaConViajes=linea.split(SPLIT_BY);

				int id=Integer.parseInt(rutaConViajes[0]);

				DoubleLinkedList<VOTripInRoute> listaViajes = new DoubleLinkedList<VOTripInRoute>();

				VOTripInRoute newTripInRoute = new VOTripInRoute(Integer.parseInt(rutaConViajes[1]), Integer.parseInt(rutaConViajes[2]), rutaConViajes[3], rutaConViajes[4], Integer.parseInt(rutaConViajes[5]), Integer.parseInt(rutaConViajes[6]), Integer.parseInt(rutaConViajes[7]), Integer.parseInt(rutaConViajes[8]), Integer.parseInt(rutaConViajes[9]));
				listaViajes.addFirst(newTripInRoute);
				listaViajes.updateActual(listaViajes.getNodeByElement(newTripInRoute));

				linea=leclinea.readLine();

				while(linea!=null && linea.startsWith(rutaConViajes[0]) )
				{
					String[] rutaConViajes2=linea.split(SPLIT_BY);

					VOTripInRoute newTripInRoute2 = new VOTripInRoute(Integer.parseInt(rutaConViajes2[1]), Integer.parseInt(rutaConViajes2[2]), rutaConViajes2[3], rutaConViajes2[4], Integer.parseInt(rutaConViajes2[5]), Integer.parseInt(rutaConViajes2[6]), Integer.parseInt(rutaConViajes2[7]), Integer.parseInt(rutaConViajes2[8]), Integer.parseInt(rutaConViajes2[9]));

					Node<VOTripInRoute> current=listaViajes.getFirst();
					int contador=0;

					if(listaViajes.getSize()>1)
					{
						while(current!=null)
						{
							if(current.darElemento().getTripID()<newTripInRoute2.getTripID())
							{
								contador++;
							}
							current=current.darSiguiente();
						}

						listaViajes.addAtk(newTripInRoute2, contador);
					}
					else if(listaViajes.getSize()==1)
					{
						if(current.darElemento().getTripID()>newTripInRoute2.getTripID())
						{
							listaViajes.addFirst(newTripInRoute2);
						}
						else
						{
							listaViajes.addAtEnd(newTripInRoute2);
						}
					}
					else
					{
						listaViajes.addFirst(newTripInRoute2);
					}


					linea=leclinea.readLine();

				}



				if(findId(id, listaRutas)!=null)
				{
					for(int i=0;i<listaViajes.getSize();i++)
					{
						findId(id, listaRutas).darElemento().getViajes().addAtk(listaViajes.getElement(i), findId(id, listaRutas).darElemento().getViajes().getSize());
					}
				}


				else
				{
					VORouteWTrips newRouteWTrips = new VORouteWTrips(id, listaViajes);
					listaRutas.addAtEnd(newRouteWTrips);

				}

				System.out.println(listaRutas.getFirst().darAnterior().darElemento().getID());
				System.out.println(listaViajes.getSize());

				for(int i=0;i<listaRutas.getFirst().darAnterior().darElemento().getViajes().getSize();i++)
				{
					System.out.println(i);
					System.out.println(listaRutas.getFirst().darAnterior().darElemento().getViajes().getSize());
					System.out.println(listaRutas.getFirst().darAnterior().darElemento().getViajes().getElement(i).getTripID());
				}
			}
			leclinea.close();
			//System.out.println(listaRutas.getSize());

		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}  
	}



	@Override
	public void loadStopTimes(String stopTimesFile) {
		// TODO Auto-generated method stub
		BufferedReader br;
		String line = "";
		try 
		{
			br = new BufferedReader(new FileReader(stopTimesFile));
			DoubleLinkedList<VOStopTime> listaStopTimes = new DoubleLinkedList<VOStopTime>();
			line = br.readLine();
			while ((line = br.readLine()) != null) 
			{
				String[] stopTime = line.split(SPLIT_BY);

				VOStopTime newStopTime = new VOStopTime(Integer.parseInt(stopTime[0]), stopTime[1], stopTime[2], Integer.parseInt(stopTime[3]), Integer.parseInt(stopTime[4]), stopTime[5], Integer.parseInt(stopTime[6]), Integer.parseInt(stopTime[7]), stopTime[8]);
				listaStopTimes.addAtEnd(newStopTime);
			} 
			br.close();
		}
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Metodo para encontrar si existe un nodo existente con el id dado
	 * @param pId
	 * @param pLista
	 * @return nodo si existe, null de lo contrario
	 */
	public Node<VOZone> findZone(String pId, RingList<VOZone> pLista)
	{
		Node<VOZone> current=pLista.getFirst();
		if(current!=null)
		{
			if(current.darElemento().getID().equals(pId))
			{
				return current;
			}
			current=current.darSiguiente();


			while(current!=pLista.getFirst() && current!=null)
			{
				if(current.darElemento().getID().equals(pId))
				{
					return current;
				}
				current=current.darSiguiente();
			}
		}
		else
		{
			return null;	
		}
		return null;
	}
	
	@Override
	public void loadStops(String stopsFile) {
		// TODO Auto-generated method stub
		FileReader lector;
		RingList<VOZone> listaZonas = new RingList<VOZone>();
		try 
		{
			lector = new FileReader( stopsFile );
			BufferedReader leclinea= new BufferedReader( lector );
			String linea=leclinea.readLine( );

			linea=leclinea.readLine( );
			
			while(linea!=null)
			{
				String[] stop=linea.split(SPLIT_BY);
				
				String zoneId=stop[6];
				
				DoubleLinkedList<VOStop> listaParadas = new DoubleLinkedList<VOStop>();

				VOStop newStopInZone = new VOStop(Integer.parseInt(stop[0]), stop[1], stop[2], stop[3], Double.parseDouble(stop[4]), Double.parseDouble(stop[5]), zoneId, stop[7], stop[8], "");
				
				listaParadas.addFirst(newStopInZone);
				listaParadas.updateActual(listaParadas.getNodeByElement(newStopInZone));

				linea=leclinea.readLine();

				while(linea!=null && linea.contains(stop[6]) )
				{
					String[] stop2=linea.split(SPLIT_BY);

					VOStop newStopInZone2 = new VOStop(Integer.parseInt(stop2[0]), stop2[1], stop2[2], stop2[3], Double.parseDouble(stop2[4]), Double.parseDouble(stop2[5]), stop2[6], stop2[7], stop2[8], "");
					listaParadas.add(newStopInZone2);
					listaParadas.updateActual(listaParadas.getNodeByElement(newStopInZone2));

					linea=leclinea.readLine();

				}


				if(findZone(zoneId, listaZonas)!=null)
				{
					for(int i=0;i<listaParadas.getSize();i++)
					{
						findZone(zoneId, listaZonas).darElemento().getParadas().addAtk(listaParadas.getElement(i), findZone(zoneId, listaZonas).darElemento().getParadas().getSize());
					}
				}
				else
				{
					VOZone newZone = new VOZone(zoneId, listaParadas);
					listaZonas.addAtEnd(newZone);
				}
			}
			leclinea.close();
			
			System.out.println(listaZonas.getSize());
			for (int i = 0; i < listaZonas.getSize(); i++) 
			{
				int contador = 0;
				for (int j = 0; j < listaZonas.getElement(i).getParadas().getSize(); j++) 
				{
					contador++;
					if(j==0)
						System.out.println(listaZonas.getElement(i).getID());
					
					if(j==listaZonas.getElement(i).getParadas().getSize()-1)
						System.out.println(contador);	
				}
			}
		} 
		catch (Exception e)
		{
			e.printStackTrace();
		}  
	}

	@Override
	public IList<VORoute> routeAtStop(String stopName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IList<VOStop> stopsRoute(String routeName, String direction) {
		// TODO Auto-generated method stub
		return null;
	}

}
