package model.data_structures;

public class Node<T> 
{
	//ATRIBUTOS
	private T elemento;
	private Node<T> siguiente;
	private Node<T> anterior;
	
	//CONSTRUCTORES
	public Node()
	{
		this(null, null, null);
	}
	
	public Node(T e, Node<T> pAnterior, Node<T> pSiguiente)
	{
		elemento = e;
		anterior = pAnterior;
		siguiente = pSiguiente;
	}
	
	//METODOS
	
	public T darElemento()
	{
		return elemento;
	}
	
	public Node<T> darSiguiente()
	{
		return siguiente;
	}
	
	public Node<T> darAnterior()
	{
		return anterior;
	}
	
	
	public void cambiarElemento(T pElemento)
	{
		elemento = pElemento;
	}
	
	public void cambiarAnterior(Node<T> pAnterior)
	{
		anterior = pAnterior;
	}
	
	public void cambiarSiguiente(Node<T> pSiguiente)
	{
		siguiente = pSiguiente;
	}
}
